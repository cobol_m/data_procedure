       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA5.
       AUTHOR. MUKKU.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 GRADE-DATA PIC X(90) VALUE "39030261WORAWIT         886345593B
      -    " 886352593D+886342193B+886478593C 886481592C+886491591A ".
       01 GRADE REDEFINES GRADE-DATA.
           03 STU-ID      PIC   9(8).
           03 STU-NAME    PIC   X(16).
           03 SUB1.
              05 COU-CODE1   PIC   9(8).
              05 COU-UNIT1   PIC   9.
              05 COU-GRADE1  PIC   X(2).
           03 SUB2.
              05 COU-CODE2   PIC   9(8).
              05 COU-UNIT2   PIC   9.
              05 COU-GRADE2  PIC   X(2).
           03 SUB3.
              05 COU-CODE3   PIC   9(8).
              05 COU-UNIT3   PIC   9.
              05 COU-GRADE3  PIC   X(2).
           03 SUB4.
              05 COU-CODE4   PIC   9(8).
              05 COU-UNIT4   PIC   9.
              05 COU-GRADE4  PIC   X(2).
           03 SUB5.
              05 COU-CODE5   PIC   9(8).
              05 COU-UNIT5   PIC   9.
              05 COU-GRADE5  PIC   X(2).
           03 SUB6.
              05 COU-CODE6   PIC   9(8).
              05 COU-UNIT6   PIC   9.
              05 COU-GRADE6  PIC   X(2).
       66 STUDENT-ID   RENAMES STU-ID.
       66 STUDENT-INFO RENAMES STU-ID  THRU STU-NAME.
       66 STUDENT-INFO1 RENAMES STU-ID  THRU SUB1.
       01 STUCODE REDEFINES GRADE-DATA.
      *01 STUCODE REDEFINES GRADE.
           05 STU-YEAR PIC 9(2).
           05 FILLER   PIC X(6).
           05 STU-SHORT-NAME PIC X(3).
       77 TOTAL-COST PIC 9(4)V99.
       PROCEDURE DIVISION.
       Begin.
      *    MOVE GRADE-DATA TO GRADE
           DISPLAY GRADE
           DISPLAY "STU-ID: " STU-ID
           DISPLAY "STUDENT-ID: " STUDENT-ID
           DISPLAY "STUDENT-INFO: " STUDENT-INFO
           DISPLAY "STUDENT-INFO1: " STUDENT-INFO1
           DISPLAY "STU-: " STU-YEAR STU-SHORT-NAME
           DISPLAY "SUBJECT 1: " SUB1 
           DISPLAY "CODE1: " COU-CODE1
           DISPLAY "UNIT1: " COU-UNIT1
           DISPLAY "GRADE1: " COU-GRADE1
           DISPLAY "SUBJECT 2: " SUB2 
           DISPLAY "CODE2: " COU-CODE2
           DISPLAY "UNIT2: " COU-UNIT2
           DISPLAY "GRADE2: " COU-GRADE2
           DISPLAY "SUBJECT 3: " SUB3 
           DISPLAY "CODE3: " COU-CODE3
           DISPLAY "UNIT3: " COU-UNIT3
           DISPLAY "GRADE3: " COU-GRADE3
           DISPLAY "SUBJECT 4: " SUB4 
           DISPLAY "CODE4: " COU-CODE4
           DISPLAY "UNIT4: " COU-UNIT4
           DISPLAY "GRADE4: " COU-GRADE4
           DISPLAY "SUBJECT 5: " SUB5 
           DISPLAY "CODE5: " COU-CODE5
           DISPLAY "UNIT5: " COU-UNIT5
           DISPLAY "GRADE5: " COU-GRADE5
           DISPLAY "SUBJECT 6: " SUB6 
           DISPLAY "CODE6: " COU-CODE6
           DISPLAY "UNIT6: " COU-UNIT6
           DISPLAY "GRADE6: " COU-GRADE6
           .